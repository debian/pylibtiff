pylibtiff (0.4.1+20160502-2) UNRELEASED; urgency=medium

  * d/rules: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:17:23 +0200

pylibtiff (0.4.1+20160502-1) unstable; urgency=medium

  * QA (group) upload
  * New checkout from Github
    Closes: #838266
  * Rewrote debian/get-orig-source to automatically detect latest change
    date
  * Document exclusion of bitarray code copy
  * Move packaging to Git
  * cme fix dpkg-control
  * Orphan the package
    Closes: #705208
  * hardening=+bindnow

 -- Andreas Tille <tille@debian.org>  Tue, 25 Oct 2016 08:50:11 +0200

pylibtiff (0.4.1~20150805-1) unstable; urgency=medium

  * New VCS checkout
    Closes: #780045
  * Migrated from GoogleCode to Github (rewrote get-orig-source)
  * d/get-orig-source: use xz compression
  * Mathieu declared to leave the team so I add myself as uploader
  * cme fix dpkg-control
  * debhelper 9
  * cme fix dpkg-copyright
  * Injected pull-requests as quilt patches

 -- Andreas Tille <tille@debian.org>  Thu, 24 Dec 2015 08:14:46 +0100

pylibtiff (0.3.0~svn78-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS: ndarraytypes.h:681:82: error: operator '<=' has no right
    operand":
    add patch numpy_deprecated_api.patch from upstream svn to handle changes
    in numpy 1.7.
    Thanks to Andrey Rahmatullin for pointing to the upstream commit.
    (Closes: #713588)

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Jul 2013 15:53:16 +0200

pylibtiff (0.3.0~svn78-3) unstable; urgency=low

  * Add support for tiff4. Closes: #663490
  * Bump Std-Vers to 3.9.3, no changes needed
  * Use my @d.o alias, remove DMUA flag for now
  * Add explicit call to dh_numpy to remove an lintian error

 -- Mathieu Malaterre <malat@debian.org>  Fri, 04 May 2012 11:46:51 +0200

pylibtiff (0.3.0~svn78-2) unstable; urgency=low

  * Remove invalid patch. Thanks to Piotr for spotting this. Closes: #649904

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Tue, 29 Nov 2011 12:03:03 +0100

pylibtiff (0.3.0~svn78-1) unstable; urgency=low

  * Initial Debian Upload (Closes: #648769)

 -- Mathieu Malaterre <mathieu.malaterre@gmail.com>  Sun, 20 Nov 2011 11:18:33 +0100
